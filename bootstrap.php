<?php

ini_set('memory_limit', '-1');

$config = include_once('./config.php');

require_once './Connect.php';
require_once './HttpAuth.php';

$connect = new Connect($config['db']['host'], $config['db']['dbname'], $config['db']['username'], $config['db']['password'], $config['db']['table']);