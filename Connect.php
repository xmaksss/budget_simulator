<?php

require_once ('./bootstrap.php');

/**
 * Connect to DataBase.
 * Insert and Select records.
 * Class Connect
 */
class Connect
{
    private $_dbh;
    private $_table;
    private $_times = 0;

    /**
     * Connect constructor.
     * @param $host
     * @param $dbname
     * @param $user
     * @param $password
     * @param $table
     * @throws Exception
     */
    function __construct($host, $dbname, $user, $password, $table)
    {
        try {
            $this->_dbh = new PDO("mysql:host={$host};dbname={$dbname}", $user, $password, [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_TIMEOUT => 120
            ]);
            $this->_dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            throw new Exception("Can't connect to {$host}, {$e->getMessage()}");
        }

        $this->_table = $table;

        $this->_checkTable();
    }

    /**
     * Create table if not exists
     * @return bool
     */
    private function _checkTable()
    {
        $results = $this->_dbh->query("SHOW TABLES LIKE '{$this->_table}'");

        if ($results->rowCount() > 0) {
            return true;
        }

        $sql = "CREATE TABLE IF NOT EXISTS {$this->_table}(id int(20) AUTO_INCREMENT PRIMARY KEY,slug VARCHAR(48) NOT NULL, data text NOT NULL,created_ad TIMESTAMP)";
        $this->_dbh->exec($sql);
    }

    /**
     * @param $data
     * @return bool
     */
    public function insert($data)
    {
        $slug = $this->_getSlug();

        $check = $this->_dbh->prepare("SELECT FROM `{$this->_table}` WHERE slug=:slug");
        $check->bindParam(':slug', $slug, PDO::PARAM_STR);

        if ($this->_times > 5) {
            return false;
        }

        if ($check->rowCount() > 0) {
            $this->_times++;
            $this->insert($data);
        }

        $stmt = $this->_dbh->prepare("INSERT INTO `{$this->_table}` (`slug`, `data`) VALUES (:slug, :rdata)");
        $stmt->bindParam(':slug', $slug, PDO::PARAM_STR);
        $stmt->bindParam(':rdata', $data, PDO::PARAM_STR);

        try {
            $stmt->execute();
            return $slug;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function load($slug)
    {
        $stmt = $this->_dbh->prepare("SELECT * FROM `{$this->_table}` WHERE `slug`=:slug");
        $stmt->bindParam(':slug', $slug, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function all($limit = 1000, $offset = 0)
    {
        $stmt = $this->_dbh->prepare("SELECT * FROM `{$this->_table}` LIMIT :offset, :limit");
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return string
     */
    public function getCount()
    {
        $result = $this->_dbh->prepare("SELECT count(*) FROM `{$this->_table}`");
        $result->execute();

        return $result->fetchColumn(0);
    }

    /**
     * @return array
     */
    public function receiveInPartsAll()
    {
        $step = 1000;
        $count = $this->getCount();
        $numberOfParts = ceil($count/$step);

        $result = [];

        for ($i = 0; $i < $numberOfParts; $i++) {
            $part = $this->all($step, $i*$step);
            $result = array_merge($result, $part);
        }

        return $result;
    }

    /**
     * @return string
     */
    private function _getSlug()
    {
        $slug = '';
        $chars = array_merge(range('a', 'z'), range(0, 9));

        for ($i = 0; $i < 32; $i++) {
            $slug .= $chars[rand(0, count($chars) - 1)];
        }

        return $slug;
    }
}