<?php

/**
 * Class HttpAuth
 */
class HttpAuth {
    protected $_login, $_password;
    protected $_func;

    /**
     * HttpAuth constructor.
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->_login = $login;
        $this->_password = $password;
    }

    /**
     * @param callable $func
     */
    public function setAction(callable $func)
    {
        $this->_func = $func;
    }

    /**
     * Check authenticated
     */
    public function checkAuth()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header('WWW-Authenticate: Basic realm="Запретная зона"');
            header('HTTP/1.0 401 Unauthorized');
            echo '<script>window.close();</script>';
            exit;
        } else {
            if ($_SERVER['PHP_AUTH_USER'] !== $this->_login || $_SERVER['PHP_AUTH_PW'] !== $this->_password) {
                header('HTTP/1.0 401 Unauthorized');
                header("Refresh:0");
                exit;
            }

            if (is_callable($this->_func)) {
                call_user_func($this->_func);
            } else {
                echo 'Callable function not found. Use "setAction" method for setting action.';
            }
        }
    }
}