<?php

require_once './bootstrap.php';

$_POST = json_decode(trim(file_get_contents('php://input')), true);

if (isset($_GET['load'])) {
    $slug = htmlspecialchars(stripslashes(trim($_GET['slug'])));

    header('Content-Type: application/json');
    echo json_encode($connect->load($slug));
} else if (isset($_POST['save'])) {
    $data = stripslashes(trim($_POST['data']));

    header('Content-Type: application/json');
    echo json_encode($connect->insert($data));
} else {
    echo 'NO';
}