<?php

require_once ('./bootstrap.php');

$login = isset($config['parser']['login']) ? $config['parser']['login'] : 'login';
$password = isset($config['parser']['password']) ? $config['parser']['password'] : 'password';

$httpAuth = new HttpAuth($login, $password);

$httpAuth->setAction(function() use ($connect) {
    $results = $connect->receiveInPartsAll();

    $titles = ['', '', ''];
    $labels = ['id', 'slug', 'date'];
    $values = [];

    if (count($results)) {
        $result = json_decode($results[0]['data'], true);
        foreach ($result as $name => $fields) {
            $titles[] = $name;
            foreach ($fields as $field => $value) {
                $titles[] = '';
                $labels[] = $field;
            }
            array_pop($titles);
        }
    }

    try {
        foreach ($results as $parts) {
            $row = [$parts['id'], $parts['slug'], $parts['created_ad']];
            foreach (json_decode($parts['data'], true) as $partName => $partData) {
                foreach ($partData as $partValueName => $v) {
                    if (isset($v['value'])) {
                        if (!is_array($v['value'])) {
                            $row[] = $v['value'];
                        } else {
                            $row[] = implode(' | ', $v['value']);
                        }
                    } else if (isset($v['values'])) {
                        if (!is_array($v['values'])) {
                            $row[] = $v['values'];
                        } else {
                            $row[] = implode(' | ', $v['values']);
                        }
                    } else {
                        if (!is_array($v)) {
                            $row[] = $v;
                        } else {
                            $row[] = implode(' | ', $v);
                        }
                    }
                }
            }

            $values[] = $row;
        }
    } catch (Exception $e) {}

    if (isset($_GET['title']) && $_GET['title'] == 1) {
        $csv = [$titles, $labels];
    } else {
        $csv = [$labels];
    }

    $csv = array_merge($csv, $values);

    $filename = "data_export_" . date("Y-m-d") . ".csv";
    $f = fopen('php://memory', 'w');
    // loop over the input array
    foreach ($csv as $line) {
        // generate csv lines from the inner arrays
        fputcsv($f, $line, ';');
    }
    // reset the file pointer to the start of the file
    fseek($f, 0);
    // tell the browser it's going to be a csv file
    header('Content-Type: application/csv');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    // make php send the generated csv lines to the browser
    fpassthru($f);
    die();
});

$httpAuth->checkAuth();